package com.example.nantha.circleviewholder;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{


    SliderLayout imageSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageSlider = (SliderLayout)findViewById(R.id.image_slider);



        HashMap<String,String> url_maps = new HashMap<>();
        url_maps.put("first", "http://ec2-52-11-165-121.us-west-2.compute.amazonaws.com/image/cache/catalog/slide-img2-762x460.jpg");
        url_maps.put("second", "http://ec2-52-11-165-121.us-west-2.compute.amazonaws.com/image/cache/catalog/slide-img1-762x460.jpg");

        for(String name : url_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView.image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra",name);

            imageSlider.addSlider(textSliderView);
        }
        //imageSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        //imageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        //imageSlider.setCustomAnimation(new DescriptionAnimation());
        //imageSlider.setDuration(4000);
     //   imageSlider.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
